import React from 'react';
import { func } from 'prop-types';
import { translate } from 'react-i18next';
import { Router } from '@reach/router';
import { Home } from 'pages';

//@TODO will clean up path mess later
import About from '../../pages/about/About.js';
import Sponsor from '../../pages/sponsor/Sponsor.js';
import Journey from '../../pages/journey/Journey.js';
import Account from '../../components/account/Account.js';
import Dashboard from '../../pages/dashboard/Dashboard.js';
import Register from '../../components/account/components/register/Register.js';
import Login from '../../components/account/components/login/Login.js';
import Analyze from '../../../src/pages/journey/components/anaylze/Analyze.js';
import Program from '../../../src/pages/journey/components/program/Program';
import Goal from '../../../src/pages/journey/components/goal/Goal.js';
import Find from '../../../src/pages/journey/components/find/Find';
import Select from '../../../src/pages/journey/components/select/Select';
import Plan from '../../../src/pages/journey/components/plan/Plan';
import Apply from '../../../src/pages/journey/components/apply/Apply';
import 'i18n/i18n';

const App = ({ t }) => {
  return (
    <Router>
      <Home path="/" />
      <About path="/about" />
      <Sponsor path="/sponsors" />
      <Dashboard path="/dashboard" />
      <Account path="/account">
        <Login path="login" />
        <Register path="register" />
      </Account>
      <Journey path="/journey">
        <Analyze path="analyze" />
        <Goal path="goal" />
        <Find path="find" />
        <Program path="program" />
        <Select path="select" />
        <Plan path="plan" />
        <Apply path="apply" />
      </Journey>
    </Router>
  );
};

App.propTypes = {
  t: func,
};

export default translate('translations')(App);
