import React from 'react';
import { translate } from 'react-i18next';
import 'i18n/i18n';
import styles from './about.scss';

import Title from 'components/title/Title';
import Navbar from 'components/navbar/Navbar';
import Footer from 'components/footer/Footer';

function About({ t }) {
  return (
    <React.Fragment>
      <Navbar />
      <div className={styles.about}>
        <div className={styles.aboutInner}>
          <div className={styles.aboutWrapper}>
            <Title id="about" title={t('about-us-title')} />
            <main className={styles.aboutDescription}>
              <p>{t('about-us-description')}</p>
            </main>
          </div>
        </div>
        <div className={styles.team}>
          <Title id="team" title={t('about-who-we-are-title')} />
          <main className={styles.teamWrapper}>
            <div className={styles.profileWrapper}>
              <div className={styles.profilePicture}> Picture </div>
              <div className={styles.profileBio}> Bio </div>
            </div>
            <div className={styles.profileWrapper}>
              <div className={styles.profilePicture}> Picture </div>
              <div className={styles.profileBio}> Bio </div>
            </div>
            <div className={styles.profileWrapper}>
              <div className={styles.profilePicture}> Picture </div>
              <div className={styles.profileBio}> Bio </div>
            </div>
          </main>
        </div>
      </div>
      <Footer />
    </React.Fragment>
  );
}
export default translate('translations')(About);
