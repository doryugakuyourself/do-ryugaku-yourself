import React from 'react';
import { render } from 'react-testing-library';
import Home from '../Home';

test('renders the Home page', () => {
  const { getByText } = render(<Home />);

  expect(getByText('Home')).toBeInTheDocument();
  expect(getByText('About Us')).toBeInTheDocument();
  expect(getByText('Sponsors')).toBeInTheDocument();
  expect(getByText('Get Started')).toBeInTheDocument();
});
