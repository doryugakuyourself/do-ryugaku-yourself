import React from 'react';
import { translate } from 'react-i18next';
import 'i18n/i18n';
import styles from './Home.scss';

import HomeSection from './components/HomeSection';
import YoutubePlayer from 'react-youtube-player';
import Navbar from 'components/navbar/Navbar';
import Footer from 'components/footer/Footer';

function Home({ t }) {
  return (
    <React.Fragment>
      <Navbar />
      <section className={styles.home}>
        <div className={styles.homeInner}>
          <header className={styles.homeContentWrapper}>
            <h1 className={styles.homeContentTitle}>
              <span>{t('home-content-title')}</span>
            </h1>
            <h2 className={styles.homeContentSubtext}>{t('home-content-subtext')}</h2>
            <a href="#mission">
              <button className={styles.homeButton}> {t('home-content-button')}</button>
            </a>
          </header>
          <div className={styles.homeVideoContainer}>
            <YoutubePlayer videoId="UFE2PXicNlg" playbackState="playing" />
          </div>
        </div>
        <main className={styles.mainContent}>
          <HomeSection id="mission" title={t('home-mission-title')} text={t('home-mission-text')} />
          <HomeSection
            id="process"
            title={t('home-overview-tile')}
            text={t('home-overview-text')}
          />
          <div className={styles.overviewWrapper}>
            <a href="/journey/analyze">
              <button className={styles.homeButton}> {t('home-overview-button')}</button>
            </a>
          </div>
        </main>
      </section>
      <Footer />
    </React.Fragment>
  );
}

export default translate('translations')(Home);
