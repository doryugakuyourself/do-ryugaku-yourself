import React from 'react';
import styles from './homeSection.scss';
import PropTypes from 'prop-types';

const HomeSection = ({ id, title, text }) => (
  <section id={id} className={styles.section}>
    <div className={styles.sectionInner}>
      <header className={styles.header}>
        <h1 className={styles.title}>{title} </h1>
      </header>
      <p className={styles.description}>{text}</p>
    </div>
  </section>
);
//why is media place  there?
HomeSection.propTypes = {
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  media: PropTypes.string.isRequired,
};

export default HomeSection;
