import React from 'react';
import { translate } from 'react-i18next';
import 'i18n/i18n';
import styles from './sponsor.scss';

import Title from 'components/title/Title';
import Navbar from 'components/navbar/Navbar';
import Footer from 'components/footer/Footer';

function Sponsor({ t }) {
  return (
    <React.Fragment>
      <Navbar />
      <section className={styles.sponsor}>
        <Title id="sponsors" title={t('sponsors-tile')} />
        <main className={styles.sponsorInner}>
          <div className={styles.sponsorWrapper}>
            <div className={styles.sponsorPicture}>Picture</div>
            <div className={styles.sponsorBio}>Bio</div>
          </div>
          <div className={styles.sponsorWrapper}>
            <div className={styles.sponsorPicture}>Picture</div>
            <div className={styles.sponsorBio}>Bio</div>
          </div>
          <div className={styles.sponsorWrapper}>
            <div className={styles.sponsorPicture}>Picture</div>
            <div className={styles.sponsorBio}>Bio</div>
          </div>
        </main>
      </section>
      <Footer />
    </React.Fragment>
  );
}
export default translate('translations')(Sponsor);
