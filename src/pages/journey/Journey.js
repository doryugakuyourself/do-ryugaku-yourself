import React from 'react';
import { translate } from 'react-i18next';
import 'i18n/i18n';
import styles from './journey.scss';
import { Router } from '@reach/router';
import Navbar from 'components/navbar/Navbar';
import Footer from 'components/footer/Footer';
import Progressbar from './components/progressbar/Progressbar';
import Analyze from './components/anaylze/Analyze';
import Goal from './components/goal/Goal';
import Program from './components/program/Program';
import Find from './components/find/Find';
import Select from './components/select/Select';
import Plan from './components/plan/Plan';
import Apply from './components/apply/Apply';

function Journey({ t }) {
  return (
    <React.Fragment>
      <Navbar />
      <div className={styles.journey}>
        <section className={styles.Progressbar}>
          <Progressbar />
        </section>
        <div className={styles.journeyMain}>
          <Router>
            <Analyze path="analyze" />
            <Goal path="goal" />
            <Find path="find" />
            <Program path="program" />
            <Select path="select" />
            <Plan path="plan" />
            <Apply path="apply" />
          </Router>
        </div>
      </div>
      <Footer />
    </React.Fragment>
  );
}

export default translate('translations')(Journey);
