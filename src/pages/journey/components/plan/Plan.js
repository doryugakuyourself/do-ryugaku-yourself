import React from 'react';
import { translate } from 'react-i18next';
import 'i18n/i18n';
import styles from './plan.scss';

import Title from 'components/title/Title';
import Button from 'components/button/Button';
function Plan({ t }) {
  return (
    <React.Fragment>
      <section className={styles.plans}>
        <Title id="plans" title={t('plan-tile')} />
        <main className={styles.planInner}>
          <div className={styles.planWrapper}>
            <div className={styles.planPicture}>Picture</div>
            <div className={styles.planBio}>Bio</div>
          </div>
          <div className={styles.planWrapper}>
            <div className={styles.planPicture}>Picture</div>
            <div className={styles.planBio}>Bio</div>
          </div>
          <div className={styles.planWrapper}>
            <div className={styles.planPicture}>Picture</div>
            <div className={styles.pageBio}>Bio</div>
          </div>
        </main>
        <Button buttonText={t('Final-Step')} buttonLink="apply" />
      </section>
    </React.Fragment>
  );
}
export default translate('translations')(Plan);
