import React from 'react';
import { translate } from 'react-i18next';
import 'i18n/i18n';
import styles from './goal.scss';

import Title from 'components/title/Title';
import Button from 'components/button/Button';

function Goal({ t }) {
  return (
    <React.Fragment>
      <section className={styles.goals}>
        <Title id="goal" title={t('goal-tile')} />
        <main className={styles.goalInner}>
          <div className={styles.goalWrapper}>
            <div className={styles.goalPicture}>Picture</div>
            <div className={styles.goalBio}>Bio</div>
          </div>
          <div className={styles.goalWrapper}>
            <div className={styles.goalPicture}>Picture</div>
            <div className={styles.goalBio}>Bio</div>
          </div>
          <div className={styles.goalWrapper}>
            <div className={styles.goalPicture}>Picture</div>
            <div className={styles.pageBio}>Bio</div>
          </div>
        </main>
        <div className={styles.buttomInner}>
          <Button buttonText={t('find-program')} buttonLink="find" />
        </div>
      </section>
    </React.Fragment>
  );
}
export default translate('translations')(Goal);
