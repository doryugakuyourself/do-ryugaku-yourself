import React from 'react';
import { Link } from '@reach/router';
import { translate } from 'react-i18next';
import 'i18n/i18n';
import styles from './progressbar.scss';

import ProgBubble from './progBubble/Progbubble';

const Progressbar = ({ t }) => (
  <div className={styles.progress}>
    <div className={styles.progressInner}>
      <div className={styles.progressWrapper}>
        <Link to="analyze">
          <ProgBubble id="analyze" title={t('self-analyze')} num={1} current={true} />
        </Link>{' '}
        <ProgBubble id="setGoals" title={t('set-goals')} num={2} current={true} />
        <ProgBubble id="findProgram" title={t('find-program')} num={3} current={false} />
        <ProgBubble id="selectSchool" title={t('choose-a-school')} num={4} current={false} />
        <ProgBubble id="requirement" title={t('check-requirements')} num={5} current={false} />
        <ProgBubble id="planning" title={t('start-planning')} num={6} current={false} />
        <ProgBubble id="apply" title={t('apply')} num={7} current={true} />
      </div>
    </div>
  </div>
);
export default translate('translations')(Progressbar);
