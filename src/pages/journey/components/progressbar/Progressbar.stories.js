import React from 'react';
import { storiesOf } from '@storybook/react';

import Progressbar from './Progressbar';

storiesOf('Progressbar', module).add('Page', () => {
  return <Progressbar />;
});
