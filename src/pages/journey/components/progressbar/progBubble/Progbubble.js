import React from 'react';
import styles from './progbubble.scss';
import PropTypes from 'prop-types';

const ProgBubble = ({ id, title, num, current }) => (
  <div id={id} className={styles.bubble}>
    <div className={styles.bubbleInner}>
      <div className={styles.bubbleWrapper}>
        <div className={styles.bubbleTitle}>{title} </div>
        <div value={current} className={styles.bubbleNum}>
          {num}
        </div>
      </div>
    </div>
  </div>
);
//why is media place  there?
ProgBubble.propTypes = {
  title: PropTypes.string.isRequired,
  num: PropTypes.number.isRequired,
  current: PropTypes.bool.isRequired,
};

export default ProgBubble;
