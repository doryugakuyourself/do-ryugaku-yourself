import React from 'react';
import { translate } from 'react-i18next';
import 'i18n/i18n';
import styles from './analyze.scss';

import Title from 'components/title/Title';
import Button from 'components/button/Button';

const Analyze = ({ t }) => (
  <div className={styles.analyze}>
    <div className={styles.analyzeInner}>
      <Title id="analyze" title={t('Self-Analyze')} />
      <div className={styles.analyzeWrapper}>
        <div className={styles.worksheetWrapper}>
          <div className={styles.worksheet}>worker</div>
          <button className={styles.worksheetButton}>{t('download')}</button>
        </div>
        <div className={styles.analyzeText}>{t('analyze-text')}</div>
      </div>
      <Button buttonText={t('Setting-Goals')} buttonLink="goal" />
    </div>
  </div>
);

export default translate('translations')(Analyze);
