import React from 'react';
import { translate } from 'react-i18next';
import 'i18n/i18n';
import styles from './select.scss';

import Title from 'components/title/Title';
import Button from 'components/button/Button';

function Select({ t }) {
  return (
    <React.Fragment>
      <section className={styles.select}>
        <Title id="selects" title={t('select-tile')} />
        <main className={styles.selectInner}>
          <div className={styles.selectWrapper}>
            <div className={styles.selectPicture}>Picture</div>
            <div className={styles.selectBio}>Bio</div>
          </div>
          <div className={styles.selectWrapper}>
            <div className={styles.selectPicture}>Picture</div>
            <div className={styles.selectBio}>Bio</div>
          </div>
          <div className={styles.selectWrapper}>
            <div className={styles.selectPicture}>Picture</div>
            <div className={styles.pageBio}>Bio</div>
          </div>
        </main>
        <Button buttonText={t('Select-Program')} buttonLink="plan" />
      </section>
    </React.Fragment>
  );
}
export default translate('translations')(Select);
