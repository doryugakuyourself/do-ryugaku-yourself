import React from 'react';
import { translate } from 'react-i18next';
import 'i18n/i18n';
import styles from './program.scss';

import Title from 'components/title/Title';
import Button from 'components/button/Button';

function program({ t }) {
  return (
    <React.Fragment>
      <section className={styles.programs}>
        <Title id="programs" title={t('program-tile')} />
        <main className={styles.programInner}>
          <div className={styles.programWrapper}>
            <div className={styles.programPicture}>Picture</div>
            <div className={styles.programBio}>Bio</div>
          </div>
          <div className={styles.programWrapper}>
            <div className={styles.programPicture}>Picture</div>
            <div className={styles.programBio}>Bio</div>
          </div>
          <div className={styles.programWrapper}>
            <div className={styles.programPicture}>Picture</div>
            <div className={styles.pageBio}>Bio</div>
          </div>
        </main>
        <Button buttonText={t('select-school')} buttonLink="select" />
      </section>
    </React.Fragment>
  );
}
export default translate('translations')(program);
