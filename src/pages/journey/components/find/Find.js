import React from 'react';
import { translate } from 'react-i18next';
import 'i18n/i18n';
import styles from './find.scss';

import Title from 'components/title/Title';
import Button from 'components/button/Button';

function Find({ t }) {
  return (
    <React.Fragment>
      <div className={styles.find}>
        <Title id="find" title={t('Find-title')} />
      </div>
      <div className={styles.buttomInner}>
        <Button buttonText={t('find-programs')} buttonLink="program" />
      </div>
    </React.Fragment>
  );
}

export default translate('translations')(Find);
