import React from 'react';
import { translate } from 'react-i18next';
import 'i18n/i18n';
import styles from './apply.scss';

import Title from 'components/title/Title';
import Button from 'components/button/Button';

function Apply({ t }) {
  return (
    <React.Fragment>
      <section className={styles.apply}>
        <Title id="applys" title={t('apply-title')} />
        <main className={styles.applyInner}>
          <div className={styles.applyWrapper}>
            <div className={styles.applyPicture}>Picture</div>
            <div className={styles.applyBio}>Bio</div>
          </div>
          <div className={styles.applyWrapper}>
            <div className={styles.applyPicture}>Picture</div>
            <div className={styles.applyBio}>Bio</div>
          </div>
          <div className={styles.applyWrapper}>
            <div className={styles.applyPicture}>Picture</div>
            <div className={styles.pageBio}>Bio</div>
          </div>
        </main>
        <div className={styles.buttomInner}>
          <Button buttonText={t('Apply-Button')} buttonLink="/" />
        </div>
      </section>
    </React.Fragment>
  );
}
export default translate('translations')(Apply);
