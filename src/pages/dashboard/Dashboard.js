import React from 'react';
import { translate } from 'react-i18next';
import 'i18n/i18n';
import styles from './dashboard.scss';

import Title from 'components/title/Title';
import Navbar from 'components/navbar/Navbar';
import Footer from 'components/footer/Footer';

function dashboard({ t }) {
  return (
    <React.Fragment>
      <Navbar />
      <section className={styles.dashboard}>
        <Title id="dashboard" title={t('dashboard-tile')} />
        <main className={styles.dashboardInner}>
          <div className={styles.dashboardWrapper}>
            <button>Continue</button>
            <p>Overview</p>
            <p>
              Journey--- Make this a horizontal bars for each sub component and when clicked it
              creates the profile need?
            </p>
            <p>Search</p>
            <p>Saved</p>
            <p>Account</p>
            <p>Setting</p>
            <p>Contact</p>
          </div>
        </main>
      </section>
      <Footer />
    </React.Fragment>
  );
}
export default translate('translations')(dashboard);
