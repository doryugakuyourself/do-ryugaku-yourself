export default function loadEn() {
  return {
    translations: {
      // Navbar ///
      // Footer //
      'footer-terms': 'Terms & Conditions',
      'footer-cookies': 'Cookies',
      'footer-privacy': 'Privacy Policy',

      // Home //
      'home-content-title': 'Do Ryguaku\n yourself',
      'home-content-subtext': 'Find the new you. Come study abroad',
      'home-content-button': 'Learn more',
      'home-mission-title': 'Our mission',
      'home-mission-text':
        'has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney       College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Malorum (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, Lorem ipsum dolor sit amet.., comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from de Finibus Bonorum et Malorum by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham',
      'home-overview-tile': 'Taking your first steps',
      'home-overview-text':
        '1.Find the reason you want to study abroad\n 2.Imagine where you want to be after your study abroad program\n 3. Find the program that best fit you\n 4. Choose a school that has your program and fits you\n 4.Familiarize yourself with the necessary requirement to get in\n 5.start planning\n 6.apply',
      'home-overview-button': 'begin your journey',

      // About US //
      'about-us-title': 'Our Vision',
      'about-us-description':
        'mission statement\n Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and printing in place of English to emphasise design elements over content.ts a convenient tool for mock-ups. It helps to outline the visual elements of a document or presentation, eg typography, font, or layout. Lorem ipsum is mostly a part of a Latin text by the classical author and philosopher Cicero. Its words and letters have been changed by addition or removal, so to deliberately render its content nonsensical its not genuine, correct, or comprehensible Latin anymore. While lorem ipsums still resembles classical Latin, it actually has no meaning whatsoever. As Cicero text does contain the letters K, W, or Z, alien to latin, these, and others are often inserted randomly to mimic the typographic appearence of European languages, as are digraphs not to be found in the original.In a professional context it often happens that private or corporate clients corder a publication to be made and presented with the actual content still not being ready. Think of a news blog that filled with content hourly on the day of going live. However, reviewers tend to be distracted by comprehensible content, say, a random text copied from a newspaper or the internet. The are likely to focus on the text, disregarding the layout and its elements. Besides, random text risks to be unintendedly humorous or offensive, an unacceptable risk in corporate environments. Lorem ipsum and its many variants have been employed since the early 1960ies, and quite likely since the sixteenth century.',
      'about-who-we-are-title': 'The Team',
      'about-who-we-are-description': 'description',
      // Sponsors //
      'sponsors-tile': 'Thank you to our supporters!',

      // Journey//
      'journey-test': 'this is where you start',

      //Progressbar//
      'progress-test': 'this is a test',

      //Anaylze//
      'analyze-text':
        'Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and printing in place of English to emphasise design elements over content.ts a convenient tool for mock-ups. It helps to outline the visual elements of a document or presentation, eg typography, font, or layout. Lorem ipsum is mostly a part of a Latin text by the classical author and philosopher Cicero. Its words and letters have been changed by addition or removal, so to deliberately render its content nonsensical its not genuine, correct, or comprehensible Latin anymore.',
    },
  };
}
