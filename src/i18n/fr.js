export default function loadFr() {
  return {
    translations: {
      'footer-terms': "Conditions d'utilisation",
      'footer-cookies': 'Cookies',
      'footer-privacy': 'Politique de confidentialité',
      'home-header': 'Do Ryugaku Yourself',
      'home-start': 'Commencez maintenant',
      'home-subheader': 'Apprenez avec des mentors partout à travers le monde',
    },
  };
}
