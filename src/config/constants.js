export default {
  contact: {
    EMAIL: process.env.REACT_APP_EMAIL || '#',
    SLACK_URL: process.env.REACT_APP_SLACK_URL || '#',
  },
  social: {
    FB_URL: process.env.REACT_APP_FACEBOOK_URL || '#',
    INSTA_URL: process.env.REACT_APP_INSTA_URL || '#',
    TWITTER_URL: process.env.REACT_APP_TWITTER_URL || '#',
  },
};
