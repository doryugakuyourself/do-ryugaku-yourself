import React from 'react';
import { translate } from 'react-i18next';
import 'i18n/i18n';
import styles from './account.scss';
import { Router } from '@reach/router';
import Login from './components/login/Login';
import Register from './components/register/Register';

function Account({ t, props }) {
  return (
    <div className={styles.account}>
      <div className={styles.accountInner}>
        <div className={styles.accountWrapper}>
          <Router>
            <Login path="login" />
            <Register path="register" />
          </Router>
        </div>
      </div>
    </div>
  );
}
export default translate('translations')(Account);
