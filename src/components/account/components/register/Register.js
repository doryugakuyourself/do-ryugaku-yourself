import React from 'react';
import { translate } from 'react-i18next';
import 'i18n/i18n';
import styles from './register.scss';

function Register({ t, props }) {
  return (
    <div className={styles.login}>
      <div className={styles.loginInner}>
        <div className={styles.loginWrapper}>
          <form action="/dashboard">
            <label>
              Username:
              <input type="text" name="test" />{' '}
            </label>
            <br />
            <label>
              Password:
              <input type="text" />
            </label>
            <br />
            <label>
              Country: <input type="text" />{' '}
            </label>
            <br />
            <label>
              Submit:
              <input type="submit" value="Submit" />
            </label>
          </form>
        </div>
      </div>
    </div>
  );
}
export default translate('translations')(Register);
