import React from 'react';
import { translate } from 'react-i18next';
import 'i18n/i18n';
import styles from './login.scss';

import Button from 'components/button/Button';

function Login({ t, props }) {
  return (
    <div className={styles.login}>
      <div className={styles.loginInner}>
        <div className={styles.loginWrapper}>
          <form action="/dashboard">
            <label>
              Username:
              <input type="text" name="test" />{' '}
            </label>
            <br />
            <label>
              Password:
              <input type="text" />
            </label>
            <br />
            <label>
              Submit:
              <input type="submit" value="Submit" />
            </label>
          </form>
          <Button buttonText={t('Facebook')} buttonLink="/dashboard" />
          <Button buttonText={t('Google')} buttonLink="/dashboard" />
          <a href="register"> new student? </a>
          <br />
          <a href="register/representative"> new university representative </a>
          <br />
          <a href="dashboard"> temp dashboard link </a>
        </div>
      </div>
    </div>
  );
}
export default translate('translations')(Login);
