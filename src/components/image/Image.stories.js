import React from 'react';
import { storiesOf } from '@storybook/react';

import Image from './Image';

storiesOf('Components/Image', module).add('Basic Image', () => {
  return (
    <React.Fragment>
      <Image src="/" alt="/" />
      <Image src="/" alt="Image not found!" />
    </React.Fragment>
  );
});
