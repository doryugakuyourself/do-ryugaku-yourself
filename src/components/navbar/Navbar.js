import React from 'react';
import { translate } from 'react-i18next';
import 'i18n/i18n';
import styles from './navbar.scss';

const Navbar = ({ t }) => (
  <nav className={styles.navigationBar}>
    <div className={styles.inner}>
      <a href="/" className={styles.navLinks}>
        {t('Home')}
      </a>
      <a href="/about" className={styles.navLinks}>
        {t('About Us')}
      </a>
      <a href="/sponsors" className={styles.navLinks}>
        {t('Sponsors')}
      </a>
      <a href="/journey/analyze" className={styles.navLinks}>
        {t('Get Started')}
      </a>
      <a href="/account/login" className={styles.navLinks}>
        {t('Login')}
      </a>
    </div>
  </nav>
);

export default translate('translations')(Navbar);
