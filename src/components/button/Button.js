import React from 'react';
import styles from './button.scss';
import PropTypes from 'prop-types';

const Button = ({ buttonText, buttonLink }) => (
  <div className={styles.button}>
    <div className={styles.buttonInner}>
      <a href={buttonLink}>
        <button className={styles.buttonStyle}>{buttonText}</button>
      </a>
    </div>
  </div>
);

Button.propTypes = {
  buttonText: PropTypes.string.isRequired,
  buttonLink: PropTypes.string.isRequired,
};

export default Button;
