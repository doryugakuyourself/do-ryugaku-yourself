import React from 'react';
import styles from './title.scss';
import PropTypes from 'prop-types';

const Title = ({ id, title }) => (
  <div id={id} className={styles.titleInner}>
    <header className={styles.header}>
      <div className={styles.title}>{title}</div>
    </header>
  </div>
);

Title.PropTypes = {
  title: PropTypes.string.isRequired,
};

export default Title;
