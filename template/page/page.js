import React from 'react';
import { translate } from 'react-i18next';
import 'i18n/i18n';
import styles from './pages.scss';

import Title from 'components/title/Title';
import Navbar from 'components/navbar/Navbar';
import Footer from 'components/footer/Footer';

function Page({ t }) {
  return (
    <React.Fragment>
      <Navbar />
      <section className={styles.pages}>
        <Title id="pages" title={t('sponsors-tile')} />
        <main className={styles.pageInner}>
          <div className={styles.pageWrapper}>
            <div className={styles.pagePicture}>Picture</div>
            <div className={styles.pageBio}>Bio</div>
          </div>
          <div className={styles.pageWrapper}>
            <div className={styles.pagePicture}>Picture</div>
            <div className={styles.pageBio}>Bio</div>
          </div>
          <div className={styles.pageWrapper}>
            <div className={styles.pagePicture}>Picture</div>
            <div className={styles.pageBio}>Bio</div>
          </div>
        </main>
      </section>
      <Footer />
    </React.Fragment>
  );
}
export default translate('translations')(Page);