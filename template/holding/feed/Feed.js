import React from 'react';
import styles from './Feed';
import PropTypes from 'prop-types';

const Feed = ({id, image, text, children}) => (
    <div className={styles.feedWrapper}>
        <div className={styles.feedInner}>
            <div className={styles.feedProfile}>
                <section id={id} className={styles.feedSection}>
                    <span className ={styles.image}>{image}</span>
                </section>
            </div>
        </div>
    </div>
 );
export default Feed;